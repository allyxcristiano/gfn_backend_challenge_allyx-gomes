# gfn_backend_challenge_allyx-gomes

# Instalação/executação do projeto 

É um projeto maven então basta importar o mesmo em sua IDE e aguardar as dependências serem baixadas. Após isto já deverá ser possível executar o projeto.

# O banco de dados

Foi utilizado o H2, se trata de um banco de dados em memória e ele inicia sem registros toda vez que a aplicação reinicia. 

Ao executar o projeto o painel simples do mesmo poderá ser acessado por: 

http://127.0.0.1:8085/h2/

# O build e deploy

Estão disponibilizados dois arquivos do tipo 'Jenkinsfile' no projeto, um para o ambiente de homologação e o outro de produção, deverá se criar um Job do tipo pipeline no servidor de automação Jenkins, configurar a branch de origem do projeto e o Jenkinsfile referente ao ambiente e o build estará pronto. O Ip que se encontra no arquivo na etapa de 'Enviando p/ Servidor' e se espera que se tenha configurado as devidas permissões de chaves públicas / privadas para que a copia do .jar ocorra sem problemas.

Também no projeto estão incluidos dois arquivos do tipo 'deploy' os mesmos estão preparados para serem executados em um servidor linux e reiniciar a aplicação nos ambientes de homologação e produção.

Será interessante configurar alguma aplicação (cron) para detectar a alteração no arquivo .jar e automaticamente executar os respectivos sh de deploy.

# As rotas

# Loja

GET http://127.0.0.1:8085/lojas - Vem paginado com padrão de 10 por página

GET http://127.0.0.1:8085/lojas?page=0&size=3 - Paginação customizada

GET http://127.0.0.1:8085/lojas?page=0&size=3&nome=alameda - Opção paginada, filtrando nome

GET http://127.0.0.1:8085/lojas/{idLoja} - {idLoja} é o identificador da loja que deverá ser informado

GET http://127.0.0.1:8085/lojas/consumo-medio-total

POST http://127.0.0.1:8085/lojas/envia-csv - Multipart upload, enviar body com key: file e value: arquivo csv

POST http://127.0.0.1:8085/lojas - Requer cliente no body, formato json

PUT http://127.0.0.1:8085/lojas - Requer cliente no body, formato json

DELETE http://127.0.0.1:8085/lojas/{idLoja} - {idLoja} é o identificador da loja que deverá ser informado

# Cliente

GET http://127.0.0.1:8085/clientes - Vem paginado com padrão de 10 por página

GET http://127.0.0.1:8085/clientes?page=0&size=3 - Paginação customizada

GET http://127.0.0.1:8085/clientes?page=0&size=3&nome=paulista - Opção paginada, filtrando nome

GET http://127.0.0.1:8085/clientes/{idCliente} - {idCliente} é o identificador do cliente que deverá ser informado

POST http://127.0.0.1:8085/clientes/envia-csv - Multipart upload, enviar body com key: file e value: arquivo csv

POST http://127.0.0.1:8085/clientes - Requer cliente no body, formato json

PUT http://127.0.0.1:8085/clientes - Requer cliente no body, formato json

DELETE http://127.0.0.1:8085/clientes/{idCliente} - {idCliente} é o identificador do cliente que deverá ser informado
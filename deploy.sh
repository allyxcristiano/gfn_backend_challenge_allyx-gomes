#!/bin/bash

#Matar todas as instancias
pkill -f .*gfn_backend_challenge_allyx-gomes-0.0.1-SNAPSHOT.jar

cd /home/java/geofusion/

java -jar -XX:+UseG1GC -Xms64m -Xmx256m -Djava.security.egd=file:/dev/./urandom -Dspring.profiles.active=default -Dserver.port=8085 gfn_backend_challenge_allyx-gomes-0.0.1-SNAPSHOT.jar &
package br.com.geofusion.gfn_backend_challenge_allyxgomes.controller;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Loja;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.service.IArmazenadorArquivoService;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.service.ILojaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

@RestController
@RequestMapping("/lojas")
public class LojaController {
    private ILojaService lojaService;
    private IArmazenadorArquivoService armazenadorArquivoService;

    @Autowired
    public LojaController(@Qualifier("lojaService") ILojaService lojaService, @Qualifier("armazenadorArquivoService") IArmazenadorArquivoService armazenadorArquivoService) {
        this.lojaService = lojaService;
        this.armazenadorArquivoService = armazenadorArquivoService;
    }

    @RequestMapping(value = "/envia-csv")
    public ResponseEntity<byte[]> enviaCsv(@RequestParam("file") MultipartFile file) {
        try {
            Path caminhoArquivo = armazenadorArquivoService.armazenarArquivo(file);
            lojaService.enviaCsv(caminhoArquivo);
            return ResponseEntity.status(HttpStatus.OK).header("sucess", "Importação de planilha csv de lojas efetuada com sucesso!").build();
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(value = "/consumo-medio-total")
    public ResponseEntity<Double> consumoMedioTotal() {
        try {
            return new ResponseEntity<>(lojaService.recuperaConsumoMedioTotal(), HttpStatus.OK);
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(method = {RequestMethod.POST}, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<byte[]> cria(@RequestBody Loja loja) {
        try {
            lojaService.cria(loja);
            return ResponseEntity.status(HttpStatus.CREATED).header("sucess", "Loja criada com sucesso!").build();
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(method = {RequestMethod.PUT}, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<byte[]> atualiza(@RequestBody Loja loja) {
        try {
            lojaService.atualiza(loja);
            return ResponseEntity.status(HttpStatus.OK).header("sucess", "Loja atualizada com sucesso!").build();
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(method = {RequestMethod.GET}, value = "/{idLoja}")
    public ResponseEntity<Loja> recupera(@PathVariable(value = "idLoja") Long idLoja) {
        try {
            return new ResponseEntity<>(lojaService.recupera(idLoja), HttpStatus.OK);
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(method = {RequestMethod.GET})
    public ResponseEntity<Page<Loja>> recupera(@PageableDefault() Pageable pageable, @RequestParam(value = "nome", defaultValue = "") String nome) {
        try {
            return new ResponseEntity<>(lojaService.recupera(nome, pageable), HttpStatus.OK);
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(method = {RequestMethod.DELETE}, value = "/{idLoja}")
    public ResponseEntity<byte[]> remove(@PathVariable(value = "idLoja") Long idLoja) {
        try {
            lojaService.remove(idLoja);
            return ResponseEntity.status(HttpStatus.OK).header("sucess", "Loja removida com sucesso!").build();
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }
}
package br.com.geofusion.gfn_backend_challenge_allyxgomes.controller;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Cliente;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.service.IArmazenadorArquivoService;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

@RestController
@RequestMapping("/clientes")
public class ClienteController {
    private IClienteService clienteService;
    private IArmazenadorArquivoService armazenadorArquivoService;

    @Autowired
    public ClienteController(@Qualifier("clienteService") IClienteService clienteService, @Autowired IArmazenadorArquivoService armazenadorArquivoService) {
        this.clienteService = clienteService;
        this.armazenadorArquivoService = armazenadorArquivoService;
    }

    @RequestMapping(value = "/envia-csv")
    public ResponseEntity<byte[]> enviaCsv(@RequestParam("file") MultipartFile file) {
        try {
            Path caminhoArquivo = armazenadorArquivoService.armazenarArquivo(file);
            clienteService.enviaCsv(caminhoArquivo);
            return ResponseEntity.status(HttpStatus.OK).header("sucess", "Importação de planilha csv de clientes efetuada com sucesso!").build();
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(method = {RequestMethod.POST}, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<byte[]> cria(@RequestBody Cliente cliente) {
        try {
            clienteService.cria(cliente);
            return ResponseEntity.status(HttpStatus.CREATED).header("sucess", "Cliente criado com sucesso!").build();
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(method = {RequestMethod.PUT}, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<byte[]> atualiza(@RequestBody Cliente cliente) {
        try {
            clienteService.atualiza(cliente);
            return ResponseEntity.status(HttpStatus.OK).header("sucess", "Cliente atualizado com sucesso!").build();
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(method = {RequestMethod.DELETE}, value = "/{idCliente}")
    public ResponseEntity<byte[]> remove(@PathVariable(value = "idCliente") Long idCliente) {
        try {
            clienteService.remove(idCliente);
            return ResponseEntity.status(HttpStatus.OK).header("sucess", "Cliente removido com sucesso!").build();
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(method = {RequestMethod.GET}, value = "/{idCliente}")
    public ResponseEntity<Cliente> recupera(@PathVariable(value = "idCliente") Long idCliente) {
        try {
            return new ResponseEntity<>(clienteService.recupera(idCliente), HttpStatus.OK);
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }

    @RequestMapping(method = {RequestMethod.GET})
    public ResponseEntity<Page<Cliente>> recupera(@PageableDefault() Pageable pageable, @RequestParam(value = "nome", defaultValue = "") String nome) {
        try {
            return new ResponseEntity<>(clienteService.recupera(nome, pageable), HttpStatus.OK);
        } catch (Throwable e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header("error", e.getMessage()).build();
        }
    }
}
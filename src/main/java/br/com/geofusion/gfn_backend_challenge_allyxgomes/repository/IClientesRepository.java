package br.com.geofusion.gfn_backend_challenge_allyxgomes.repository;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClientesRepository extends JpaRepository<Cliente, Long> {
}
package br.com.geofusion.gfn_backend_challenge_allyxgomes.repository;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Loja;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ILojasRepository extends JpaRepository<Loja, Long> {

    @Query(" select loja, cliente from Loja loja "
            + " left join fetch loja.clientes cliente")
    List<Loja> recuperarLojasComClientes();

    @Query(" select loja, cliente from Loja loja "
            + " left join fetch loja.clientes cliente "
            + " where loja.nome like '%:nome%'")
    List<Loja> recuperarLojasComClientesPorNome(@Param("nome") String nome);

    @Query(" select loja, cliente from Loja loja "
            + " left join fetch loja.clientes cliente "
            + " where loja.id = :idLoja ")
    Loja recuperarPorId(@Param("idLoja") Long idLoja);

    @Query(value = " select SUM(a.faturamento) / count(b.id) from loja a "
            + " left join cliente b ON b.loja_id = a.id ", nativeQuery = true)
    double recuperarConsumoMedioTotal();
}
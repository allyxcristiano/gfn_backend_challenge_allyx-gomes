package br.com.geofusion.gfn_backend_challenge_allyxgomes.repository;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClientesPaginadosRepository extends PagingAndSortingRepository<Cliente, Long> {
    Page<Cliente> findAllByNomeIgnoreCaseOrLoja_NomeIgnoreCaseContains(String nome, String loja_nome, Pageable pageable);
}
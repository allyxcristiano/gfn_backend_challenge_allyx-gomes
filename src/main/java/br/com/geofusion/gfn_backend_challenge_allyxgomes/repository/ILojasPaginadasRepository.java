package br.com.geofusion.gfn_backend_challenge_allyxgomes.repository;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Loja;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILojasPaginadasRepository extends PagingAndSortingRepository<Loja, Long> {
    Page<Loja> findAllByNomeIgnoreCaseContains(String nome, Pageable pageable);
}
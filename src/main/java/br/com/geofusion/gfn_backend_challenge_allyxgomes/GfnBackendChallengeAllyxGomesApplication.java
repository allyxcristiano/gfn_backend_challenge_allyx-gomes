package br.com.geofusion.gfn_backend_challenge_allyxgomes;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@EnableCaching
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class GfnBackendChallengeAllyxGomesApplication {
    public static void main(String[] args) {
        SpringApplication.run(GfnBackendChallengeAllyxGomesApplication.class, args);
    }
}

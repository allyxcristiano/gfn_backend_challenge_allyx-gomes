package br.com.geofusion.gfn_backend_challenge_allyxgomes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@ToString
@Entity
public class Loja {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String nome;

    @NotNull
    private String cidade;

    @NotNull
    private String estado;

    private double latitude;

    private double longitude;

    @OneToMany(mappedBy = "loja", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Cliente> clientes;

    @NotNull
    private BigDecimal faturamento;

    public BigDecimal getConsumoMedio() {
        if (clientes == null || clientes.isEmpty()) {
            return BigDecimal.ZERO;
        }

        BigDecimal quantidadeClientes = new BigDecimal(this.getClientes().size());
        return this.getFaturamento().divide(quantidadeClientes, 2);
    }
}
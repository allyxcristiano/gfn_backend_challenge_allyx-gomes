package br.com.geofusion.gfn_backend_challenge_allyxgomes.model;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.util.DistanceUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ToString
@Entity
public class Cliente {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String nome;

    @NotNull
    private String cidade;

    @NotNull
    private String estado;

    private double latitude;

    private double longitude;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Loja loja;

    public String getNomeLoja() {
        return this.loja.getNome();
    }

    public void relacionaLoja(List<Loja> lojas) {
        Loja lojaMaisProxima = lojas.get(0);
        double menorDistancia = DistanceUtil.distance(this.getLatitude(), lojaMaisProxima.getLatitude(), this.getLongitude(), lojaMaisProxima.getLongitude(), 0.00, 0.00);

        for (Loja loja : lojas) {
            double distanciaLojaAtual = DistanceUtil.distance(this.getLatitude(), loja.getLatitude(), this.getLongitude(), loja.getLongitude(), 0.00, 0.00);
            if (distanciaLojaAtual < menorDistancia) {
                menorDistancia = distanciaLojaAtual;
                lojaMaisProxima = loja;
            }
        }

        this.setLoja(lojaMaisProxima);
    }
}
package br.com.geofusion.gfn_backend_challenge_allyxgomes.service.validation;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Cliente;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Loja;

import java.util.List;

public interface IClienteValidation {
    void validaCriacao(Cliente cliente, List<Loja> lojas);

    void validaAtualizacao(Cliente cliente, List<Loja> lojas);

    void validaRecuperacao(Cliente cliente);

    void validaRemocao(Cliente cliente);
}

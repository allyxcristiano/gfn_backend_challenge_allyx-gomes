package br.com.geofusion.gfn_backend_challenge_allyxgomes.service.validation;

public interface IArmazenadorArquivoValidation {
    void validarNomeArquivo(String nomeArquivo);
}
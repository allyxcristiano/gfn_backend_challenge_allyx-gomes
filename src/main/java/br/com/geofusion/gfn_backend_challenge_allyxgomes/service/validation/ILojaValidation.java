package br.com.geofusion.gfn_backend_challenge_allyxgomes.service.validation;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Loja;

public interface ILojaValidation {
    void validaCriacao(Loja loja);

    void validaAtualizacao(Loja loja);

    void validaRecuperacao(Loja loja);

    void validaRemocao(Loja loja);
}
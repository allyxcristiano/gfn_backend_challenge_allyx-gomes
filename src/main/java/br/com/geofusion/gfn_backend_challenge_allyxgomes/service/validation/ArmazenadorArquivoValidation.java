package br.com.geofusion.gfn_backend_challenge_allyxgomes.service.validation;

import org.springframework.stereotype.Component;

import javax.validation.ValidationException;

@Component
public class ArmazenadorArquivoValidation implements IArmazenadorArquivoValidation {
    @Override
    public void validarNomeArquivo(String nomeArquivo) {
        if (nomeArquivo.contains("..")) {
            throw new ValidationException("Nome do arquivo contém caracteres inválidos " + nomeArquivo);
        }
    }
}
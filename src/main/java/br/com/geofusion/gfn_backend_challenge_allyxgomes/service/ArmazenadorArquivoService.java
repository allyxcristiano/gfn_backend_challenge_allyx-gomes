package br.com.geofusion.gfn_backend_challenge_allyxgomes.service;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.property.FileStorageProperties;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.service.validation.IArmazenadorArquivoValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class ArmazenadorArquivoService implements IArmazenadorArquivoService {
    private Path fileStorageLocation;
    private IArmazenadorArquivoValidation armazenadorArquivoValidation;

    @Autowired
    public ArmazenadorArquivoService(FileStorageProperties fileStorageProperties, @Qualifier("armazenadorArquivoValidation") IArmazenadorArquivoValidation armazenadorArquivoValidation) throws IOException {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        this.armazenadorArquivoValidation = armazenadorArquivoValidation;
        Files.createDirectories(this.fileStorageLocation);
    }

    @Override
    public Path armazenarArquivo(MultipartFile file) throws IOException {
        String nomeArquivo = StringUtils.cleanPath(file.getOriginalFilename());
        this.armazenadorArquivoValidation.validarNomeArquivo(nomeArquivo);
        Path caminhoArquivoLocalizado = this.fileStorageLocation.resolve(nomeArquivo);
        Files.copy(file.getInputStream(), caminhoArquivoLocalizado, StandardCopyOption.REPLACE_EXISTING);
        return caminhoArquivoLocalizado;
    }
}
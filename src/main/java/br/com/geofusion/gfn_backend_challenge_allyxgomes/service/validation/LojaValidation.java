package br.com.geofusion.gfn_backend_challenge_allyxgomes.service.validation;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Loja;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;

@Component
public class LojaValidation implements ILojaValidation {
    @Override
    public void validaCriacao(Loja loja) {
        if (loja == null) {
            throw new ValidationException("Loja nula");
        }

        if (loja.getId() != null) {
            throw new ValidationException("Loja já tem id preenchido");
        }

        if (loja.getFaturamento() == null) {
            throw new ValidationException("Loja com faturamento nulo");
        }

        if (loja.getNome() == null) {
            throw new ValidationException("Loja com nome nulo");
        }

        if (loja.getCidade() == null) {
            throw new ValidationException("Loja com cidade nula");
        }

        if (loja.getEstado() == null) {
            throw new ValidationException("Loja com estado nulo");
        }
    }

    @Override
    public void validaAtualizacao(Loja loja) {
        if (loja == null) {
            throw new ValidationException("Loja nula");
        }

        if (loja.getId() == null || (loja.getId() != null && loja.getId() == 0)) {
            throw new ValidationException("Loja precisa ter id para ser atualizada");
        }

        if (loja.getFaturamento() == null) {
            throw new ValidationException("Loja com faturamento nulo");
        }

        if (loja.getNome() == null) {
            throw new ValidationException("Loja com nome nulo");
        }

        if (loja.getCidade() == null) {
            throw new ValidationException("Loja com cidade nula");
        }

        if (loja.getEstado() == null) {
            throw new ValidationException("Loja com estado nulo");
        }
    }

    @Override
    public void validaRecuperacao(Loja loja) {
        if (loja == null) {
            throw new ValidationException("Loja não encontrada");
        }
    }

    @Override
    public void validaRemocao(Loja loja) {
        if (loja == null) {
            throw new ValidationException("Loja não encontrada");
        }
    }
}
package br.com.geofusion.gfn_backend_challenge_allyxgomes.service.validation;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Cliente;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Loja;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;
import java.util.List;

@Component
public class ClienteValidation implements IClienteValidation {
    @Override
    public void validaCriacao(Cliente cliente, List<Loja> lojas) {
        if (cliente == null) {
            throw new ValidationException("Cliente nulo");
        }

        if (cliente.getId() != null && cliente.getId() > 0) {
            throw new ValidationException("Cliente já tem id preenchido");
        }

        if (cliente.getNome() == null) {
            throw new ValidationException("Cliente com nome nulo");
        }

        if (cliente.getCidade() == null) {
            throw new ValidationException("Cliente com cidade nula");
        }

        if (cliente.getEstado() == null) {
            throw new ValidationException("Cliente com estado nulo");
        }

        if (lojas == null || lojas.isEmpty()) {
            throw new ValidationException("Nâo é possível cadastrar um cliente sem ter lojas para o relacionar");
        }
    }

    @Override
    public void validaAtualizacao(Cliente cliente, List<Loja> lojas) {
        if (cliente == null) {
            throw new ValidationException("Cliente nula");
        }

        if (cliente.getId() == null || (cliente.getId() != null && cliente.getId() == 0)) {
            throw new ValidationException("Cliente precisa ter id para ser atualizada");
        }

        if (cliente.getNome() == null) {
            throw new ValidationException("Cliente com nome nulo");
        }

        if (cliente.getCidade() == null) {
            throw new ValidationException("Cliente com cidade nula");
        }

        if (cliente.getEstado() == null) {
            throw new ValidationException("Cliente com estado nulo");
        }

        if (lojas == null || lojas.isEmpty()) {
            throw new ValidationException("Nâo é possível cadastrar um cliente sem ter lojas para o relacionar");
        }
    }

    @Override
    public void validaRecuperacao(Cliente cliente) {
        if (cliente == null) {
            throw new ValidationException("Cliente não encontrada");
        }
    }

    @Override
    public void validaRemocao(Cliente cliente) {
        if (cliente == null) {
            throw new ValidationException("Cliente não encontrada");
        }
    }
}
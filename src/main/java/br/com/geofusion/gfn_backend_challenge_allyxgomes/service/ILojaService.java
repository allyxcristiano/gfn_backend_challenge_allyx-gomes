package br.com.geofusion.gfn_backend_challenge_allyxgomes.service;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Loja;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;

public interface ILojaService {
    void enviaCsv(Path caminhoArquivo) throws IOException;

    BigDecimal getConsumoMedioTotal();

    BigDecimal getConsumoMedio(String nomeLoja);

    void cria(Loja cliente);

    void atualiza(Loja cliente);

    Loja recupera(Long idLoja);

    Page<Loja> recupera(String nome, Pageable pageable);

    void remove(Long idLoja);

    Double recuperaConsumoMedioTotal();
}
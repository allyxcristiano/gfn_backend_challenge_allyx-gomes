package br.com.geofusion.gfn_backend_challenge_allyxgomes.service;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Cliente;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Loja;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.repository.IClientesPaginadosRepository;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.repository.IClientesRepository;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.repository.ILojasRepository;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.service.validation.IClienteValidation;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Service
@NoArgsConstructor
public class ClienteService implements IClienteService {
    private IClientesRepository clientesRepository;
    private ILojasRepository lojasRepository;
    private IClientesPaginadosRepository clientesPaginadosRepository;
    private IClienteValidation clienteValidation;

    @Autowired
    public ClienteService(@Qualifier("clienteValidation") IClienteValidation clienteValidation, IClientesRepository clientesRepository, ILojasRepository lojasRepository, IClientesPaginadosRepository clientesPaginadosRepository) {
        this.clienteValidation = clienteValidation;
        this.clientesRepository = clientesRepository;
        this.lojasRepository = lojasRepository;
        this.clientesPaginadosRepository = clientesPaginadosRepository;
    }

    @Override
    @Async
    public void enviaCsv(Path caminhoArquivo) throws IOException {
        Reader reader = Files.newBufferedReader(caminhoArquivo);

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(',')
                .build();

        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();

        String[] linha;

        while ((linha = csvReader.readNext()) != null) {
            Cliente cliente = new Cliente();
            cliente.setNome(linha[0]);
            cliente.setCidade(linha[1]);
            cliente.setEstado(linha[2]);
            cliente.setLatitude(Double.parseDouble(linha[3]));
            cliente.setLongitude(Double.parseDouble(linha[4]));

            this.cria(cliente);
        }
    }

    @Override
    public void cria(Cliente cliente) {
        List<Loja> lojas = lojasRepository.findAll();
        clienteValidation.validaCriacao(cliente, lojas);
        cliente.relacionaLoja(lojas);
        clientesRepository.save(cliente);
    }

    @Override
    public void atualiza(Cliente cliente) {
        List<Loja> lojas = lojasRepository.findAll();
        clienteValidation.validaAtualizacao(cliente, lojas);
        cliente.relacionaLoja(lojas);
        clientesRepository.save(cliente);
    }

    @Override
    public Cliente recupera(Long idCliente) {
        Cliente cliente = clientesRepository.findOne(idCliente);
        clienteValidation.validaRecuperacao(cliente);
        return cliente;
    }

    @Override
    public void remove(Long idCliente) {
        Cliente cliente = this.recupera(idCliente);
        clienteValidation.validaRemocao(cliente);
        clientesRepository.delete(idCliente);
    }

    @Override
    public Page<Cliente> recupera(String nome, Pageable pageable) {
        return clientesPaginadosRepository.findAllByNomeIgnoreCaseOrLoja_NomeIgnoreCaseContains(nome, nome, pageable);
    }
}
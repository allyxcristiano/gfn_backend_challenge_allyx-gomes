package br.com.geofusion.gfn_backend_challenge_allyxgomes.service;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.nio.file.Path;

public interface IClienteService {
    void enviaCsv(Path caminhoArquivo) throws IOException;

    void cria(Cliente cliente);

    void atualiza(Cliente cliente);

    Cliente recupera(Long idCliente);

    void remove(Long idCliente);

    Page<Cliente> recupera(String nome, Pageable pageable);
}

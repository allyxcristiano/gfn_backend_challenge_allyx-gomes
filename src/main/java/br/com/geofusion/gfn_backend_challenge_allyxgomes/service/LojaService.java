package br.com.geofusion.gfn_backend_challenge_allyxgomes.service;

import br.com.geofusion.gfn_backend_challenge_allyxgomes.model.Loja;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.repository.ILojasPaginadasRepository;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.repository.ILojasRepository;
import br.com.geofusion.gfn_backend_challenge_allyxgomes.service.validation.ILojaValidation;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
@NoArgsConstructor
public class LojaService implements ILojaService {
    private ILojasRepository lojasRepository;
    private ILojasPaginadasRepository lojasPaginadasRepository;
    private ILojaValidation lojaValidation;

    @Autowired
    public LojaService(@Qualifier("lojaValidation") ILojaValidation lojaValidation, ILojasRepository lojasRepository, ILojasPaginadasRepository lojasPaginadasRepository) {
        this.lojaValidation = lojaValidation;
        this.lojasRepository = lojasRepository;
        this.lojasPaginadasRepository = lojasPaginadasRepository;
    }

    @Override
    @Async
    public void enviaCsv(Path caminhoArquivo) throws IOException {
        Reader reader = Files.newBufferedReader(caminhoArquivo);

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(',')
                .build();

        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();

        String[] linha;

        while ((linha = csvReader.readNext()) != null) {
            Loja loja = new Loja();
            loja.setNome(linha[0]);
            loja.setCidade(linha[1]);
            loja.setEstado(linha[2]);
            loja.setLatitude(Double.parseDouble(linha[3]));
            loja.setLongitude(Double.parseDouble(linha[4]));
            loja.setFaturamento(new BigDecimal(linha[5]));

            this.cria(loja);
        }
    }

    @Override
    public BigDecimal getConsumoMedioTotal() {
        return null;
    }

    @Override
    public BigDecimal getConsumoMedio(String nomeLoja) {
        return null;
    }

    @Override
    public void cria(Loja loja) {
        lojaValidation.validaCriacao(loja);
        lojasRepository.save(loja);
    }

    @Override
    public void atualiza(Loja loja) {
        lojaValidation.validaAtualizacao(loja);
        lojasRepository.save(loja);
    }

    @Override
    public Loja recupera(Long idLoja) {
        Loja loja = lojasRepository.recuperarPorId(idLoja);
        lojaValidation.validaRecuperacao(loja);
        return loja;
    }

    @Override
    public Page<Loja> recupera(String nome, Pageable pageable) {
        return lojasPaginadasRepository.findAllByNomeIgnoreCaseContains(nome, pageable);
    }

    @Override
    public void remove(Long idLoja) {
        Loja loja = this.recupera(idLoja);
        lojaValidation.validaRemocao(loja);
        lojasRepository.delete(idLoja);
    }

    @Override
    public Double recuperaConsumoMedioTotal() {
        return lojasRepository.recuperarConsumoMedioTotal();
    }
}
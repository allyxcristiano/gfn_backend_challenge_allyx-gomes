package br.com.geofusion.gfn_backend_challenge_allyxgomes.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;

public interface IArmazenadorArquivoService {
    Path armazenarArquivo(MultipartFile file) throws IOException;
}
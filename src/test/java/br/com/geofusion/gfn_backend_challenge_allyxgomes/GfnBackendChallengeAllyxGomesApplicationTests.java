package br.com.geofusion.gfn_backend_challenge_allyxgomes;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GfnBackendChallengeAllyxGomesApplicationTests {

    @Test
    public void contextLoads() {
    }

}
